#!/bin/bash
cd `dirname $0`

# Check arguments
bpurge=0
if [ "$1" == "-p" ] || [ "$1" == "--purge" ]; then
  bpurge=1
  shift
fi
bquiet=0
if [ "$1" == "-q" ] || [ "$1" == "--quiet" ]; then
  bquiet=1
  shift
fi

# Set up CORAL/COOL CMake for LCG releases or nightlies
. setupLCG.sh $*
status=$?
if [ "$status" != "0" ]; then
  echo ""
  echo "Usage: $0 [-p|--purge] [-q|--quiet] [<arguments to setupLCG.sh>]"
  exit $status
fi

# Additional settings for Makefile and ninja verbosity
export VERBOSE=1
export CMAKEFLAGS="-DCMAKE_VERBOSE_MAKEFILE=ON $CMAKEFLAGS"

# Check out the logs directory if it does not exist
if [ ! -d logs ]; then
  if [ `more CMakeLists.txt 2>&1 | grep "project("` == "project(CORAL)" ]; then
    gitproj=lcgcoral
  else
    gitproj=lcgcool
  fi
  echo "WARNING! logs directory does not exist: check it out from git  $gitproj "
  klist > /dev/null 2>&1
  if [ "$?" == "0" ]; then
    git clone --recursive https://:@gitlab.cern.ch:8443/$gitproj/logs.git
  else
    echo "no kerberos found trying https"
    git clone --recursive https://gitlab.cern.ch/$gitproj/logs.git
  fi
  if [ ! -d logs/cmake ]; then
    echo "ERROR! logs/cmake is missing?"
    exit 1
  fi
fi

# Default make arguments and build directory location
makevars=
blddir=build.${BINARY_TAG}

# Remove the build, install and log directories [but not the ccache cache!]
if [ "$bpurge" == "1" ]; then
  echo "=== make purge"
  make purge ${makevars}
fi

# Output to file only if this is a clean rebuild, else output to stdout
if [ "${BINARY_TAG}" != "" ] && [ ! -d ${blddir} ] && [ ! -d ${BINARY_TAG} ]; then
  out=logs/cmake/log-cmake-${BINARY_TAG}.txt
  \rm -rf $out
elif [ "$bquiet" == "1" ]; then
  out=/dev/null
else
  out=/dev/stdout
fi
touch $out

# Dump the cache for info before rebuilding
if ccache -h > /dev/null 2>&1; then 
  echo "=== ccache -s"
  echo ccache is `which ccache`
  ccache -s # Dump this only to stdout
fi

# Rebuild from scratch
echo "=== make ${makevars}"
echo "Output is $out"
date >> $out
time make ${makevars} -j8 >> $out 2>&1 # redirect warnings from stderr
status=$?
if [ "$status" != "0" ]; then 
  echo; echo "*** ERROR! make failed ***"; echo
  if [ "${out#logs/}" != "$out" ]; then
    grep -i error $out
  fi
  exit $status
fi
date >> $out
echo "=== make ${makevars} install"
echo "Output is $out"
if [ "$out" == "/dev/stdout" ]; then
  time make ${makevars} install
else
  time make ${makevars} install >> $out #2>&1
fi
date >> $out

# Parse output file for build warnings
# [NB ccache does reproduce build warnings also for cached files!]
if [ "${out#logs/}" != "$out" ]; then
  echo "=== [build warning search]"
  grep warning $out > /dev/null 2>&1 
  if [ "$?" != "0" ]; then
    echo "No build warnings found"
  else
    echo "WARNING! Build warnings found!"
    grep warning $out | grep -v "warning: argument unused during compilation"
    grep "warning: argument unused during compilation" $out > /dev/null 2>&1 
    if [ "$?" == "0" ]; then
      echo "[Omitted all occurrences of 'warning: argument unused during compilation']"
    fi
  fi
fi

# Dump CMake based environment
pushd cmake > /dev/null
if [ -f .internal/dump_env.sh ]; then
  .internal/dump_env.sh > .internal/env_dump_build.txt
  .internal/dump_env.sh -i > .internal/env_dump_install.txt
fi
popd > /dev/null

# TEMPORARY! Compare CMT and CMake based environments
# Skip this step if neither AFS nor CVMFS are available (e.g. on ARM)
#if [ -d /afs ] || [ -d /cvmfs ]; then
#  pushd cmake > /dev/null
#  if [ -f .internal/dump_env.sh ]; then
#    .internal/dump_env.sh -c > .internal/env_comparison_build.txt
#    .internal/dump_env.sh -c -i > .internal/env_comparison_install.txt
#  fi
#  popd > /dev/null
#fi
