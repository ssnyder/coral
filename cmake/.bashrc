# Determine the path to this .bashrc
# Determine the project (CORAL or COOL) from the top-level CMakeLists.txt
###project="xenv "
###if [ "$BASH_SOURCE" != "" ]; then
###  topdir=`dirname ${BASH_SOURCE}`
###  topdir=`cd ${topdir}; pwd`
###  project=`ls ${topdir}/*.xenv`
###  project=`basename ${project} .xenv`
###fi
###PS1='\[\e[7;34m\][\u@\h '$project'\s]\[\e[0m\]\[\e[1;34m\] \w > \[\e[0m\]'

# Minimal bash setup
if [ `uname` == "Darwin" ]; then
  alias 'ls=ls -aFG'
  shopt -s checkwinsize # See http://unix.stackexchange.com/a/167911
  PS1='\[\e[7;34m\][\u@\h \s]\[\e[0m\]\[\e[1;34m\] \W > \[\e[0m\]'
else
  alias 'ls=ls -aF --color=tty'
  PS1='\[\e[7;34m\][\u@\h \s]\[\e[0m\]\[\e[1;34m\] \w > \[\e[0m\]'
fi
alias 'rm=rm -i'
alias 'mv=mv -i'
alias 'cp=cp -i'
alias 'lo=exit 0'
