###############################################################################
#
# Simple Makefile wrapper over cmake
#
# Author: Andrea Valassi, 31 August 2016
#
# Targets:
#   cmake   -> create cmake config ("cmake")
#   all     -> also build code ("make")
#   install -> also install code ("make install")
#
# Mandatory prerequisites:
#   - the cmake executable
#   - the BINARY_TAG env variable
#     (otherwise the cmake step will fail, this is required by CMakeLists.txt)
#
# Recommended prerequisites:
#   - the CMAKEFLAGS variable
#     (this contains the command line options that are used in the cmake step)
#   - the CMAKE_PREFIX_PATH env variable
#     (this is used by CMake to resolve package dependencies of CORAL and COOL)
#
# Optional settings:
#   - the BUILDDIR variable can be used to choose a different build dir
#     (the default BUILDDIR is build.$BINARY_TAG)
#
# All mandatory and recommended prerequisites are fulfilled if BINARY_TAG
# is set and the setupLCG.sh script is sourced before using this Makefile.
#
# *** NB: This Makefile is _NOT_ used in any way by lcgcmake. ***
# Some of the options defined in setupLCG.sh are duplicated in lcgcmake.
# 
###############################################################################

# Require BINARY_TAG to be set a priori
ifndef BINARY_TAG
  $(error BINARY_TAG is not set)
else
  ifeq ($(shell printenv BINARY_TAG),)
    $(error Environment variable BINARY_TAG is not set)
  endif
endif

# Define the default build directory as build.${BINARY_TAG}
BUILDDIR := $(CURDIR)/build.$(shell printenv BINARY_TAG)

# Dump the path to the cmake executable
$(info cmake is $(shell which cmake))

#-------------------------------------------------------------------------------
# Targets
#-------------------------------------------------------------------------------

# Phony targets
.PHONY: cmake

# Execute the make step (default target)
all: cmake
	cmake --build $(BUILDDIR) --config .

# Execute the cmake step
cmake:
ifeq ($(wildcard $(BUILDDIR)/Makefile),)
ifeq ($(wildcard $(BUILDDIR)/build.ninja),)
	cmake $(CMAKEFLAGS) -H. -B$(BUILDDIR)
endif
endif

# Execute the install step
install: cmake
	cmake --build $(BUILDDIR) --config . --target install | grep -v "^-- Up-to-date:"
