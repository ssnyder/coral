# Required external packages
if (LCG_python3 STREQUAL on) # CORALCOOL-2976
  find_package(PythonLibs REQUIRED 3)
else()
  find_package(PythonLibs REQUIRED)
endif()
include_directories(${PYTHON_INCLUDE_DIRS})

include(CORALModule)
# FIXME! PyCoral builds fail on Python3 (CORALCOOL-2977)
if (NOT LCG_python3 STREQUAL on) # CORALCOOL-2977
  CORALModule(LIBS lcg_CoralCommon ${PYTHON_LIBRARIES} HEADERS)
endif()

# Copy and install python modules into the build and install areas
include(CORALConfigPython)
coral_install_python_modules()

#----------------------------------------------------------------------------
# PyCoral unit tests
#----------------------------------------------------------------------------

include(CORALConfigScripts)
macro(pycoral_unittest dir test)
  copy_and_install_program(${test} tests/${dir} tests/bin tests/bin)
endmacro()

# The 'import coral' test
pycoral_unittest(AttributeList test_PyCoralUnit_AttributeList.py)
