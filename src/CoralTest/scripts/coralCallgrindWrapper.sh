#! /bin/bash

# Check mandatory input arguments (executable command and its arguments)
if [ "$1" == "" ]; then
  echo "Usage $0 <executable> [<args>]"
  exit 1
fi
cmd=$1
shift
args="$*"
cmdlog=$cmd
while [ "$1" != "" ]; do cmdlog=${cmdlog}_${1//\//_}; shift; done

# Check if the specified command exists
which $cmd > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command $cmd"
  exit 1
fi

# Check if valgrind exists
which valgrind > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command valgrind"
  exit 1
fi

# Check if kcachegrind exists
which kcachegrind > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command kcachegrind"
  exit 1
fi

# Fix "valgrind: failed to start tool 'memcheck' for platform 'amd64-linux'"
# relocatability issues by setting VALGRIND_LIB (thanks to Rolf!)
# [NB already set via CMT/CMake; redundantly set here for native valgrind use]
binvalgrind=`which valgrind`
binvalgrind=`dirname $binvalgrind`
if [ ! -d $binvalgrind/../lib/valgrind ]; then
  echo "ERROR! Directory $binvalgrind/../lib/valgrind does not exist!"
  exit 1
fi
export VALGRIND_LIB=`cd $binvalgrind/../lib/valgrind; pwd`

# Define the logfile
logdir=/tmp/$USER/coralCallgrind
mkdir -p $logdir > /dev/null 2>&1
if [ ! -d $logdir ]; then
  echo "WARNING! $logdir does not exist: create a temporary directory"
  logdir=`mktemp -d`
fi
if [ ! -d $logdir ]; then
  echo "ERROR! $logdir does not exist"
  exit 1
fi
\rm -f $logdir/${cmdlog}

# Run the specified command through callgrind
date
###threads=
threads="--separate-threads=yes"
time valgrind -v --tool=callgrind ${threads} --callgrind-out-file=$logdir/${cmdlog} ${cmd} ${args}
status=$?
date

# Disaply the results with kcachegrind if callgrind was successful
if [ "$status" != "0" ]; then
  echo "ERROR! valgrind/callgrind failed"
  exit 1
fi
kcachegrind --geometry 1200x800+10+40 $logdir/${cmdlog} &
