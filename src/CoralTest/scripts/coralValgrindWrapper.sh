#! /bin/bash

# Check optional input arguments (logfile)
if [ "$1" == "-log" ] && [ "$2" != "" ]; then
  log="--log-file=${2}"
  shift
  shift
else
  log=
fi

# Check mandatory input arguments (executable command and its arguments)
if [ "$1" == "" ]; then
  echo "Usage $0 [-log <logfile>] <executable> [<args>]"
  exit 1
fi
cmd=$1
shift
args="$*"
while [ "$1" != "" ]; do shift; done

# Check if the specified command exists
which $cmd > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command $cmd"
  exit 1
fi

# Check if valgrind exists
which valgrind > /dev/null 2>&1
if [ "$?" != "0" ]; then
  echo "ERROR! Unknown command valgrind"
  exit 1
fi

# Fix "valgrind: failed to start tool 'memcheck' for platform 'amd64-linux'"
# relocatability issues by setting VALGRIND_LIB (thanks to Rolf!)
# [NB already set via CMT/CMake; redundantly set here for native valgrind use]
binvalgrind=`which valgrind`
binvalgrind=`dirname $binvalgrind`
if [ ! -d $binvalgrind/../lib/valgrind ]; then
  echo "ERROR! Directory $binvalgrind/../lib/valgrind does not exist!"
  exit 1
fi
export VALGRIND_LIB=`cd $binvalgrind/../lib/valgrind; pwd`

# Set the path to the suppression file (no longer set via CMT!)
CORAL_TESTSUITE_VALGRIND_SUPP=`dirname $0`
CORAL_TESTSUITE_VALGRIND_SUPP=`cd ${CORAL_TESTSUITE_VALGRIND_SUPP}; pwd`/valgrind.supp
if [ ! -f "${CORAL_TESTSUITE_VALGRIND_SUPP}" ]; then
  echo "ERROR! Suppression file not found: ${CORAL_TESTSUITE_VALGRIND_SUPP}" 
  exit 1
fi

# Try to add valgrind suppressions for ROOT if ROOTSYS is defined
if [ -d ${ROOTSYS} ] && [ -f ${ROOTSYS}/etc/valgrind-root.supp ]; then
  rootsupp=--suppressions=${ROOTSYS}/etc/valgrind-root.supp
else
  rootsupp=
fi

# Run the specified command through valgrind
# [NB ROOT team (ROOT-7510) recommend --show-reachable=no instead]
date
time valgrind -v --leak-check=full --show-reachable=yes --error-limit=no ${log} --suppressions=${CORAL_TESTSUITE_VALGRIND_SUPP} ${rootsupp} --gen-suppressions=all --num-callers=50 --track-origins=yes ${cmd} ${args}
date
