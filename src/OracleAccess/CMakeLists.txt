find_package(Boost REQUIRED date_time)

# Workaround for CORALCOOL-2857: only enable Oracle on x86_64
IF(BINARY_TAG MATCHES "x86_64")
  find_package(Oracle REQUIRED)
  include_directories(${ORACLE_INCLUDE_DIRS})
  include(CORALModule)
  CORALModule(LIBS lcg_CoralCommon ${ORACLE_LIBRARIES} ${Boost_LIBRARIES}
              TESTS BulkInserts
                    Connection
                    DataEditor
                    DataDictionary
                    DateAndTime
                    Dual
                    GroupBy
                    MultipleSchemas
                    MultipleSessions
                    MultiThreading
                    Schema
                    SimpleQueries
                    Views
                    )
ENDIF()
