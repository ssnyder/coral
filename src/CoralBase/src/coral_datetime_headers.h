#ifndef CORALBASE_CORALDATETIMEHEADERS_H
#define CORALBASE_CORALDATETIMEHEADERS_H 1

// NB: coral_datetime_headers should be _completely_ removed in all branches!

// FIXME! This is only needed for boost::posix_time, should use coral::TimeStamp
// NB: boost_datetime_headers should be _completely_ removed in CORAL3 branch!
#include "CoralBase/../src/boost_datetime_headers.h" // SHOULD BE REMOVED!

#endif // CORALBASE_CORALDATETIMEHEADERS_H
