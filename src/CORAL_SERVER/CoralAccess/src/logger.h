#ifndef CORALACCESS_LOGGER_H
#define CORALACCESS_LOGGER_H 1

// Logger source
#define LOGGER_NAME "CORAL/RelationalPlugins/coral"

// Include files
#include "CoralServerBase/logger.h"

#endif // CORALACCESS_LOGGER_H
