#ifndef CORALSERVER_SESSIONANDFLAG_H
#define CORALSERVER_SESSIONANDFLAG_H 1

// Include files
//#include <iostream> // debug CORALCOOL-2946 and CORALCOOL-2948
#include <memory>
#include "CoralServerBase/InternalErrorException.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/SessionException.h"

namespace coral
{

  namespace CoralServer
  {

    /** @class SessionAndFlag
     *
     *  This is just a pair { std::shared_ptr<ISessionProxy>, bool }.
     *  It is needed to register both in the IObjectStoreMgr.
     *  The shared pointer ensures ISessionProxy* is alive (CORALCOOL-2946).
     *  The weak pointer ensured it is not alive too long (CORALCOOL-2948).
     *
     *  @author Andrea Valassi
     *  @date   2009-02-02
     *///

    class SessionAndFlag
    {

    public:

      /// Constructor
      SessionAndFlag( std::shared_ptr<ISessionProxy> session,
                      bool fromProxy,
                      bool weak = false )
        : m_session( weak ? NULL : session )
        , m_sessionWeak( weak ? session : NULL )
        , m_weak( weak )
        , m_fromProxy( fromProxy )
      {
        //std::cout << "++++++++++ Create " << ( weak ? "WEAK " : "" ) << "SAF " << this << std::endl;
      }

      /// Copy constructor
      /// (NB This may be optimized out by copy-elision)
      SessionAndFlag( const SessionAndFlag& rhs )
        : m_session( rhs.m_session )
        , m_sessionWeak( rhs.m_sessionWeak )
        , m_weak( rhs.m_weak )
        , m_fromProxy( rhs.m_fromProxy )
      {
        //std::cout << "++++++++++ Copy-create " << ( m_weak ? "WEAK " : "" ) << "SAF " << this << std::endl;
      }

      /// Destructor
      virtual ~SessionAndFlag()
      {
        //std::cout << "++++++++++ Delete " << ( m_weak ? "WEAK " : "" ) << "SAF " << this << std::endl;
        if ( !m_weak ) m_session.reset();
        else m_sessionWeak.reset();
      }

      /// Return by value a "weak" copy of this object (CORALCOOL-2948)
      SessionAndFlag weak()
      {
        //std::cout << "++++++++++ Return weak SAF from " << this << std::endl;
        if ( this->m_weak ) // This should never happen...
          throw InternalErrorException( "PANIC! Weak copy of a weak object?",
                                        "SessionAndFlag::weak",
                                        "coral::CoralServer" );
        return SessionAndFlag( this->m_session, this->m_fromProxy, true );
      }

      /// The session
      std::shared_ptr<ISessionProxy> session() const
      {
        //std::cout << "++++++++++ Return session for SAF " << this << std::endl;
        std::shared_ptr<ISessionProxy> session( m_weak ?
                                                m_sessionWeak.lock() :
                                                m_session );
        if ( ! session )
        {
          //std::cout << "++++++++++ INVALID SESSION session for SAF " << this << std::endl;
          throw ConnectionException( "coral::CoralServer",
                                     "SessionAndFlag::session",
                                     "Session is no longer valid" );
        }
        return session;
      }

      /// The flag
      bool fromProxy() const
      {
        return m_fromProxy;
      }

    private:

      /// Default constructor does not exist
      SessionAndFlag() = delete;

      /// Assignment operator does not exist
      SessionAndFlag& operator=( const SessionAndFlag& ) = delete;

    private:

      /// A shared pointer to the session.
      std::shared_ptr<ISessionProxy> m_session;

      /// A weak pointer to the session.
      std::weak_ptr<ISessionProxy> m_sessionWeak;

      /// Is this a "weak" copy?
      bool m_weak;

      /// Is there a CORAL server proxy between client and server?
      bool m_fromProxy;

    };

  }

}
#endif // CORALSERVER_SESSIONANDFLAG_H
