//--------------------------------------------------------------------------
// File and Version Information:
// 	$Id: NetEndpointAddress.cpp,v 1.2.2.1 2010-05-26 08:12:41 avalassi Exp $
//
// Description:
//	Class NetEndpointAddress...
//
// Author List:
//      Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------------
// This Class's Header --
//-----------------------
#include "NetEndpointAddress.h"
#include "CoralServerBase/portmap.h"

//-----------------
// C/C++ Headers --
//-----------------

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "MsgLogger.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

//		----------------------------------------
// 		-- Public Function Member Definitions --
//		----------------------------------------

namespace coral {
namespace CoralServerProxy {

// return actual port number possibly going through portmapper
unsigned short NetEndpointAddress::resolvePort() const
{
    unsigned short port = 0;
    if (m_port.pmapPrognum() == 0) {
        // do not resolve
        port = m_port.port();
    } else {
        port = coral::pmapGetPort(address(), m_port.pmapPrognum(), m_port.pmapVersion());
        PXY_INFO("Resolved port " << m_port << " -> " << port);
    }
    return port;
}

// register with a portmapper, only if program number is defined
bool NetEndpointAddress::registerPort(unsigned short port, bool override, const std::string& pmapLockDir) const
{
    bool rc = true;
    if (m_port.hasPortmap()) {
      try {
        // register our port in portmapper
        pmapRegister(port, m_port.pmapPrognum(), m_port.pmapVersion(), override, pmapLockDir);
        PXY_INFO("Registered port with portmapper " << port << " -> " << m_port);
      } catch (const std::exception& exc) {
        PXY_ERR("Failed to register port: " << exc.what());
        rc = false;
      }
    }
    return rc;
}

// unregister from a portmapper, only if program number is defined
bool NetEndpointAddress::unregisterPort(unsigned short port) const
{
    bool rc = true;
    if (m_port.hasPortmap()) {
      try {
        // unregister our prog/version from portmapper
        if (pmapUnregister(m_port.pmapPrognum(), m_port.pmapVersion(), port)) {
          PXY_INFO("Unregistered port " << m_port << " from portmapper ["
                   << m_port.pmapPrognum() << "." << m_port.pmapVersion() << "]");
        } else {
          PXY_INFO("Did not unregister from portmapper [" << m_port.pmapPrognum()
                   << "." << m_port.pmapVersion() <<
                   "], registration may have been removed or overridden");
        }
      } catch (const std::exception& exc) {
        PXY_ERR("Failed to unregister port: " << exc.what());
        rc = false;
      }
    }
    return rc;
}


} // namespace CoralServerProxy
} // namespace coral
