//--------------------------------------------------------------------------
// File and Version Information:
//  $Id: coralServerProxy.cpp,v 1.1.2.4.2.2 2012-07-03 12:32:11 avalassi Exp $
//
// Description:
//  Simple client application that makes rackets and sends them to server.
//  Supposed to work with coralEchoServer (or a proxy between them).
//
// Environment:
//  This software was developed for the ATLAS collaboration.
//
// Author List:
//  Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <ctype.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "src/NetEndpointAddress.h"
#include "src/NetSocket.h"
#include "src/MsgLogger.h"
#include "src/Packet.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using std::cout;
using std::cerr;
using std::endl;

using namespace coral::CoralServerProxy;

namespace {

NetPort server_port(40007);
bool send_connect = false;
std::string client_name = "client";
int delay_ms = -1;

void usage(const char* argv0, std::ostream& out = cout)
{
    out << "Usage: " << argv0 << " [options] server-host [test-name]\n"
            << "  Available options:\n"
            << "    -h              - print help message and exit\n"
            << "    -p number       - server port number [def: " << server_port << "]\n"
            << "    -c              - send first ConnectRO packet, needed for proxy [def: no]\n"
            << "    -n string       - client name [def: " << client_name << "]\n"
            << "    -d milliseconds - global response delay in milliseconds [def: " << delay_ms << "]\n"
            << "    -v/q            - increase/decrease verbosity (can use many times)\n";
}

NetSocket serverConnect(const NetEndpointAddress& server_address);

int doOnePacket(NetSocket& sock, coral::CALOpcode opcode, std::string const& payload,
                unsigned requestId, int responseDelay);

struct PacketData {
    coral::CALOpcode opcode;
    std::string payload;
    int responseDelay;
};


PacketData onePacket[] = {
    {coral::CALOpcode(0xa), "<SOME QUERY>", -1},
    {coral::CALOpcode(0x0), "", -1}
};

PacketData twoPacket[] = {
    {coral::CALOpcode(0xa), "<QUERY 1>", -1},
    {coral::CALOpcode(0xa), "<QUERY 2>", 0},
    {coral::CALOpcode(0x0), "", -1}
};

}

int main(int argc, char** argv)
{
    // all command options
    unsigned int verbosity = 2;

    // parse the options
    int c;
    while ((c = getopt(argc, argv, "hp:cn:d:vq")) != -1) {
        switch (c) {
        case 'h':
            usage(argv[0], cout);
            return 0;
        case 'p':
            ::server_port = NetPort::parse(optarg);
            if (! server_port.isValid()) {
              cerr << "Failed to parse -p option value: '" << optarg << "'\n";
              return 1 ;
            }
            break;
        case 'v':
            verbosity++;
            break;
        case 'c':
            ::send_connect = true;
            break;
        case 'n':
            ::client_name = optarg;
            break;
        case 'd':
            ::delay_ms = atoi(optarg);
            break;
        case 'q':
            if (verbosity > 0)
                verbosity--;
            break;
        case '?':
        default:
            usage(argv[0], cerr);
            return 1;
        }
    }

    coral::MsgLevel levels[] = {coral::Error, coral::Warning, coral::Info,
        coral::Debug, coral::Verbose};
    unsigned nLevels = sizeof levels / sizeof levels[0];
    if (verbosity >= nLevels) verbosity = nLevels - 1;
    coral::MessageStream::setMsgVerbosity(levels[verbosity]);

    // should have one or two positional arguments
    if (argc == optind) {
        cerr << "missing positional arguments\n";
        usage(argv[0], cerr);
        return 1;
    } else if (argc > optind + 2) {
        cerr << "unexpected positional arguments\n";
        usage(argv[0], cerr);
        return 1;
    }

    std::string const server_host = argv[optind];
    std::string test_name = "default";
    if (argc > optind + 1) {
        test_name = argv[optind + 1];
    }

    // resolve  server
    NetEndpointAddress server_address;
    try {
        server_address = NetEndpointAddress(server_host, ::server_port);
    } catch (const std::exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    try {

        NetSocket sock = serverConnect(server_address);
        if (not sock.isOpen())
            return 1;

        if (send_connect) {
            doOnePacket(sock, coral::CALOpcodes::ConnectRO, "<CONNECT>", 0, 0);
        }

        ::PacketData* packets = 0;
        if (test_name == "default" || test_name == "one-packet") {
            packets = ::onePacket;
        } else if (test_name == "two-packet") {
            packets = ::twoPacket;
        }

        if (packets == 0) {
            cerr << "unexpected test name: " << test_name << "\n";
            return 1;
        }

        // send/receive every packet
        for (unsigned requestId = 1; true; ++ requestId, ++ packets) {
            if (packets->opcode == 0) break;
            int delay = packets->responseDelay;
            if (delay < 0) delay = ::delay_ms;
            int stat = doOnePacket(sock, packets->opcode, packets->payload, requestId, delay);
            if (stat != 0) break;
        }

        PXY_INFO(::client_name + ": closing server connection");
        sock.shutdown();
        sock.close();

    } catch (std::exception& e) {
        std::cerr << "C++ Exception : " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "Unhandled exception " << std::endl;
        return 1;
    }

    return 0;
}

namespace {

int setSocketOptions(NetSocket& sock)
{
    // reuse the address
    if (sock.setSocketOptions(SOL_SOCKET, SO_REUSEADDR, 1) < 0) {
        return -1;
    }
    if (sock.setSocketOptions(SOL_SOCKET, SO_KEEPALIVE, 1) < 0) {
        return -1;
    }
    // disable Naggle algorithm
    if (sock.setSocketOptions(IPPROTO_TCP, TCP_NODELAY, 1) < 0) {
        return -1;
    }
    return 0;
}

NetSocket serverConnect(const NetEndpointAddress& server_address)
{
    // create socket
    PXY_DEBUG(::client_name + ": Creating socket for server connection");
    NetSocket sock(PF_INET, SOCK_STREAM, 0);
    if (not sock.isOpen()) {
        PXY_ERR(::client_name + ": Failed to create a socket: " << strerror(errno));
        return sock;
    }

    // set socket options
    if (setSocketOptions(sock) < 0) {
        PXY_ERR(::client_name + ": Failed to set socket options " << sock << ": " << strerror(errno));
        sock.close();
        return sock;
    }

    // now try to connect
    PXY_INFO(::client_name + ": connecting to a server " << server_address);
    if (sock.connect(server_address) < 0) {
        PXY_ERR(::client_name + ": Failed to connect to a server (" << server_address << "): " << strerror(errno));
        sock.close();
        return sock;
    }

    // OK
    return sock;
}


PacketPtr
makePacket(coral::CALOpcode opcode, std::string const& payload, unsigned requestId, int responseDelay)
{
    char delaybuf[32] = "";
    int delaylen = 0;
    if (responseDelay >= 0) {
        snprintf(delaybuf, sizeof delaybuf, ";DELAY_MS=%d", responseDelay);
        delaylen = strlen(delaybuf);
    }

    int bufSize = Packet::HEADERS_SIZE + payload.size() + delaylen;
    unsigned char* buf = new unsigned char[bufSize];

    // copy payload
    std::copy(payload.data(), payload.data() + payload.size(), buf + Packet::HEADERS_SIZE);
    std::copy(delaybuf, delaybuf + delaylen, buf + Packet::HEADERS_SIZE + payload.size());

    // make CAL header
    new (buf + coral::CTLPACKET_HEADER_SIZE) coral::CALPacketHeader(opcode, false, true, payload.size());

    // compute checksum for CAL+payload
    uint32_t checksum = coral::CTLPacketHeader::computeChecksum(
                    buf + coral::CTLPACKET_HEADER_SIZE, bufSize - coral::CTLPACKET_HEADER_SIZE);

    // fill CTL header
    unsigned clientId = 1;
    new (buf) coral::CTLPacketHeader(coral::CTLOK, bufSize, requestId, clientId, 0, false, checksum);

    return PacketPtr(new Packet(buf, Packet::Request));
}

int
doOnePacket(NetSocket& sock, coral::CALOpcode opcode, std::string const& payload,
            unsigned requestId, int responseDelay)
{
    PacketPtr packet = makePacket(opcode, payload, requestId, responseDelay);

    // send a packet
    PXY_INFO(::client_name + ": sending packet: " << *packet);
    int stat = packet->write(sock);
    if (stat == 0) {
        PXY_ERR(::client_name + ": server closed connection while sending a packet");
        return -1;
    } else if (stat < 0) {
        PXY_ERR(::client_name + ": failed sending a packet: " << strerror(errno));
        return -1;
    }

    // read one packet back
    PacketPtr response = Packet::read(sock, Packet::Reply);
    if (not response) {
        PXY_ERR(::client_name + ": failed receiving a packet: " << strerror(errno));
        return -1;
    }
    PXY_INFO(::client_name + ": received packet: " << *response);
    return 0;
}

}
